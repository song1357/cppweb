#include <webx/menu.h>
#include <dbentity/T_XG_GROUP.h>

class EditGroup : public webx::ProcessBase
{
protected:
	int process();
};

HTTP_WEBAPP(EditGroup)

int EditGroup::process()
{
	param_string(flag);
	param_string(icon);
	param_string(name);
	param_name_string(id);

	checkLogin();
	checkSystemRight();

	int res = 0;
	CT_XG_GROUP tab;
	sp<DBConnect> dbconn = webx::GetDBConnect();

	tab.init(dbconn);
	tab.id = id;

	if (flag == "A")
	{
		if (icon.empty()) icon = "/res/img/note/group.png";

		tab.name = name;
		tab.icon = icon;
		tab.enabled = 2;
		tab.statetime.update();
		
		if ((res = tab.insert()) > 0)
		{
			json["icon"] = tab.icon.val();
			json["id"] = tab.id.val();
		}
	}
	else if (flag == "D")
	{
		if (!tab.find())
		{
			res = XG_SYSERR;
		}
		else if (tab.next())
		{
			if (tab.enabled.val() < 2)
			{
				res = XG_AUTHFAIL;
			}
			else
			{
				res = tab.remove();
			}
		}
		else
		{
			res = XG_NOTFOUND;
		}
	}
	else
	{
		res = XG_PARAMERR;
	}

	json["code"] = res;
	out << json;
	
	return XG_OK;
}